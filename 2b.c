#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>

int main(int argc, char * argv[]){
	int rank,size;
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	MPI_Status stat;
	int a = 0, b=0;
	if (rank == 0){
		a = 5;
		MPI_Send(&a, 1, MPI_INT, 1, 0, MPI_COMM_WORLD);
		MPI_Recv(&b, 1, MPI_INT, 1, 0, MPI_COMM_WORLD, &stat);
	}
	else if (rank == 1){
		b = 4;
		MPI_Send(&b, 1, MPI_INT, 0, 0, MPI_COMM_WORLD);
		MPI_Recv(&a, 1, MPI_INT, 0, 0, MPI_COMM_WORLD, &stat);
	}
	printf("rank %d has a = %d and b = %d\n", rank,a,b);
	return 0;
}
